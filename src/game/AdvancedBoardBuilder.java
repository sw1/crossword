package game;


import java.util.Iterator;

import board.Board;
import board.BoardCell;
import board.BoardCell.Location;
import board.BoardCell.Position;
import board.BoardCell.State;
import dictionary.CwEntry;
import dictionary.Entry;
import dictionary.InteliCwDB;
import dictionary.CwEntry.Direction;

public class AdvancedBoardBuilder extends BoardBuilder{
	public AdvancedBoardBuilder(int width, int height) {
		super(width, height);
	}

	public boolean findEntry(Crossword cw) throws StrategyException {
		Board board=cw.getBoard();
		InteliCwDB icwdb=cw.getCwDB();
		Iterator<CwEntry> iter=cw.getROEntryIter();
		CwEntry cur=null;
		CwEntry newCwE=null;
		Entry newE=null;
		String pattern=null;
		//^a..(.s.?(.o)?)?$ 
		while(iter.hasNext())
		{
			cur=iter.next();
		}
		if(cur==null)
		{
			newE=icwdb.getRandom(".{2,"+String.valueOf(board.getWidth()-1)+"}");
			if(newE==null)
				throw new StrategyException();
			newCwE=new CwEntry(newE.getWord(),newE.getWord(),0,0,Direction.HORIZ);
			updateBoard(newCwE);
			return true;
		}
		else if(cur.getY()+2<board.getHeight() && cur.getDir()==Direction.HORIZ)
		{
			if(cur.getX()+cur.getWord().length()+3<board.getWidth())
			{
				newE=icwdb.getRandom(".{2,"+String.valueOf(board.getWidth()-1-cur.getX()-cur.getWord().length())+"}");
				if(newE==null)
					throw new StrategyException();
				newCwE=new CwEntry(newE.getWord(),newE.getWord(),cur.getX()+cur.getWord().length()+1,cur.getY(),Direction.HORIZ);
			}
			else
			{
				int y=cur.getY()+2;
				newE=icwdb.getRandom(".{2,"+String.valueOf(board.getWidth()-1)+"}");
				if(newE==null)
					throw new StrategyException();
				newCwE=new CwEntry(newE.getWord(),newE.getWord(),0,y,Direction.HORIZ);
			}
		}
		else if(cur.getY()+2>=board.getHeight() && cur.getDir()==Direction.HORIZ)
		{
			int x=0,y=0;
			
			while(newE==null&& x<board.getWidth())
			{
				y=0;
				while(newE==null&&y<board.getHeight()-3)
				{
					pattern=board.createPattern(x, y, x, board.getHeight()-1);
					newE=icwdb.getRandom(pattern);
					y+=2;
				}
				x++;
			}
			if(newE==null)
				return false;
			newCwE=new CwEntry(newE.getWord(),newE.getWord(),x,y,Direction.VERT);
		}
		else if(cur.getDir()==Direction.VERT)
		{
			if(board.getHeight()-2-cur.getY()>2)
			{
				int x=cur.getX(),y=cur.getY()+2;
				while(newE==null&&x<board.getHeight()){
					while(newE==null&&y<board.getHeight()-3)
					{
						pattern=board.createPattern(x, y, x, board.getHeight()-1);
						newE=icwdb.getRandom(pattern);
						y+=2;
					}
					x++;
					y=0;
				}
				if(newE==null)
					return false;
				newCwE=new CwEntry(newE.getWord(),newE.getWord(),x, y,Direction.VERT);
			}
			else if(cur.getX()+2<board.getWidth())
			{
				int x=cur.getX(),y=0;
				while(newE==null&&x<board.getHeight())
				{
					while(newE==null&&y<board.getHeight()-3)
					{
						pattern=board.createPattern(x, y, x, board.getHeight()-1);
						newE=icwdb.getRandom(pattern);
						y+=2;
					}
					x++;
					y=0;
				}
				if(newE==null)
					return false;
				newCwE=new CwEntry(newE.getWord(),newE.getWord(),x, y,Direction.VERT);
			}
		}
		if(newCwE!=null){
			updateBoard(newCwE);
			return true;
		}
		return false;
	}

	public void updateBoard(CwEntry e) {
		int l;
		BoardCell bc=null;
		if(e.getDir()==Direction.HORIZ)
		{
			l=0;
			for(int i=e.getX(); i<e.getWord().length()+e.getX();i++)
			{
				bc=new BoardCell();
				bc.options.setPosition(Position.Horiz);
				bc.options.setState(State.enabled);
				bc.setContent(String.valueOf(e.getWord().charAt(l)));
				if(i==e.getX())
					bc.options.setLocation(Location.Start);
				else if(i<e.getWord().length()+e.getX())
					bc.options.setLocation(Location.Inner);
				else
					bc.options.setLocation(Location.End);
				b.setCell(i, e.getY(), bc);
				l++;
			}
				
		}
		else
		{
			l=0;
			for(int i=e.getY(); i<e.getWord().length()+e.getY();i++)
			{
				bc=new BoardCell();
				bc.options.setPosition(Position.Vert);
				bc.options.setState(State.enabled);
				bc.setContent(String.valueOf(e.getWord().charAt(l)));
				if(i==e.getY())
					bc.options.setLocation(Location.Start);
				else if(i<e.getWord().length()+e.getY())
					bc.options.setLocation(Location.Inner);
				else
					bc.options.setLocation(Location.End);
				b.setCell(e.getX(), i, bc);
				l++;
			}
		}
	}

}
