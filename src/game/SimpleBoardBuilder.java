package game;

import java.util.Iterator;

import board.BoardCell;
import board.BoardCell.Location;
import board.BoardCell.Position;
import board.BoardCell.State;
import dictionary.CwEntry;
import dictionary.CwEntry.Direction;
import dictionary.Entry;
import dictionary.InteliCwDB;

public class SimpleBoardBuilder extends BoardBuilder {

	public SimpleBoardBuilder(int width, int height) {
		super(width, height);
	}


	public boolean findEntry(Crossword cw) throws StrategyException {
		InteliCwDB db = cw.getCwDB();
		Iterator<CwEntry> iter = entries.listIterator();
		CwEntry cwe = null, newcwe = null, headword = null;
		Entry entry = null;
		int y = 0;
		while (iter.hasNext()) {
			cwe = iter.next();
			if (cwe.getDir() == Direction.HORIZ)
				y += 1;
		}
		if (cwe == null) {

			entry = db.getRandom(b.getHeight());
			if (entry == null)
				throw new StrategyException();
			newcwe = new CwEntry(entry.getWord(), entry.getClue(), 0, 0,
					Direction.VERT);
		} else {
			iter = entries.listIterator();
			headword = iter.next();
			if (y < headword.getWord().length()) {
				entry = db.getRandom("(?i)" + headword.getWord().charAt(y)
						+ ".{0," + String.valueOf(b.getWidth() - 1) + "}");
				if (entry == null)
					newcwe = new CwEntry(String.valueOf(headword.getWord()
							.charAt(y)), "", 0, y, Direction.HORIZ);
				else
					newcwe = new CwEntry(entry.getWord(), entry.getClue(), 0,
							y, Direction.HORIZ);
			}
		}
		if (newcwe != null) {
			updateBoard(newcwe);
			return true;
		}
		return false;
	}

	public void updateBoard(CwEntry e) {
		entries.add(e);
		BoardCell c = null;
		for (int i = 0; i < e.getWord().length(); i++) {
			c = new BoardCell();
			c.setContent(e.getWord().substring(i, i + 1));
			c.options.setState(State.enabled);
			if (e.getDir() == Direction.HORIZ) {
				if (i == 0)
					c.options.setLocation(Location.Start);
				else if (i == e.getWord().length() - 1)
					c.options.setLocation(Location.End);
				else
					c.options.setLocation(Location.Inner);
				c.options.setPosition(Position.Horiz);
				b.setCell(i, e.getY(), c);
			} else if (e.getDir() == Direction.VERT) {
				c.options.setLocation(Location.Start);
				c.options.setPosition(Position.Vert);
				b.setCell(0, i, c);
			}
		}
	}

}
