package game;

import java.io.Serializable;
import java.util.LinkedList;

import dictionary.CwEntry;
import board.Board;

public abstract class BoardBuilder implements Serializable{
	protected Board b;
	protected LinkedList<CwEntry> entries = new LinkedList<CwEntry>();
	
	public BoardBuilder(int width, int height){
		b=new Board(width,height);
	}
	
	public Board getBoard() {
		return b;
	}
	public LinkedList<CwEntry> getEntries() {
		return entries;
	}
	
	public class StrategyException extends Exception{
		private static final long serialVersionUID = -659282209399600091L;}
    public abstract boolean findEntry(Crossword cw) throws StrategyException;
    public abstract void updateBoard(CwEntry e);

}

