package game;

import game.BoardBuilder.StrategyException;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.io.Serializable;
import java.lang.UnsupportedOperationException;

import dictionary.*;
import board.*;

public class Crossword implements Serializable {
	private static final long serialVersionUID = 8082097647125559767L;
	private LinkedList<CwEntry> entries;
	private Board b;
	private InteliCwDB cwdb;
	private long ID = -1;// - domyślnie -1
	private BoardBuilder boardBuilder;
	
	
	public Crossword() {
		entries=new LinkedList<>();
	}

	public class EntryIter implements Iterator<CwEntry> {
		ListIterator<CwEntry> iter;

		public EntryIter() {
			iter = entries.listIterator();
		}

		public boolean hasNext() {
			return iter.hasNext();
		}

		public CwEntry next() {
			return iter.next();
		}

		public void remove() {
			UnsupportedOperationException e = new UnsupportedOperationException();
			throw e;
		}

	}
	public Iterator<CwEntry> getROEntryIter()
	{
		return new EntryIter();
	}

	public Board getBoardCopy() {
		Board board = new Board(b.getWidth(), b.getHeight());
		for (int i = 0; i < board.getWidth(); i++) {
			for (int j = 0; j < board.getHeight(); j++) {
				board.setCell(i, j, b.getCell(i, j).copy());
			}
		}
		return board;
	}

	public InteliCwDB getCwDB() {
		return cwdb;
	}

	public void setCwDB(InteliCwDB cwdb) {
		this.cwdb = cwdb;
	}


	public final void generate() throws StrategyException {
		while (boardBuilder.findEntry(this)!= false) {
		}
		b=boardBuilder.getBoard();
		entries=boardBuilder.getEntries();
	}
	
	public void setBoardBuilder(BoardBuilder strategy){
		this.boardBuilder=strategy;
	}

	public Board getBoard() {
		return b;
	}
	
	public void setBoard(Board board)
	{
		this.b=board;
	}

	public long getID() {
		return ID;
	}
	
	public void setID(long ID){
		this.ID=ID;
	}

}
