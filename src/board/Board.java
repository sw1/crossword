package board;

import java.io.Serializable;
import java.util.LinkedList;

import board.BoardCell.Location;

public class Board implements Serializable {
	private static final long serialVersionUID = -5716149114625990869L;
	private int width, height;
    private BoardCell[][] board;
    public Board(int width, int height)
    {
    	this.width=width;
    	this.height=height;
    	board=new BoardCell[width][height];
    }
    public int getWidth()
    {
    	return width;
    }
    public int getHeight()
    {
    	return height;
    }
    public BoardCell getCell(int x, int y)
    {
    	if(x<width && y<height && x>=0 && y>=0)
    		return board[x][y];
    	return null; 
    }
    public void setCell(int x, int y, BoardCell c)
    {
    	if(x<width && y<height && x>=0 && y>=0)
    		board[x][y]=c;
    }
    public LinkedList<BoardCell> getStartCells()
    {
    	LinkedList<BoardCell> startCells=new LinkedList<BoardCell>();
    	for(int i=0;i<board.length;i++)
    	{
    		for(int j=0;j<board[i].length;j++)
    		{
    			if(board[i][j].options.getLocation()==Location.Start)
    				startCells.add(board[i][j]);
    		}
    	}
    	return startCells;
    }
    public String createPattern(int fromx, int fromy, int tox, int toy)
    {
    	StringBuffer regexp=new StringBuffer();
    	int k=0;
    	regexp.append("^");
    	if(fromx<width && tox <width && fromy<height && toy<height && fromx>=0 && tox>=0 && fromy>=0 && toy>=0)
    	{
    		for(int i=fromx;i<=tox;i++)
    		{
    			for(int j=fromy;j<=toy;j++)
    			{
    				if(board[i][j]==null)
    				{
    					if(tox==fromx&&tox>0&&board[tox-1][j]!=null)
    					{
    						j=toy+1;
    						i=tox+1;
    					}
    					else
    						regexp.append(".?");
    				}
    				else if(j<=toy)
    				{
    					if (i>fromx || j>fromy)
    					{
    						
    						if(regexp.charAt(regexp.length()-1)=='?')
    						{
    							regexp.setCharAt(regexp.length()-2, '(');
    							regexp.setCharAt(regexp.length()-1, '.');
    							regexp.append(board[i][j].getContent());
    							k++;
    						}
    						else{
    							regexp.append(board[i][j].getContent());
    						
    						}
    					}
    					else
    						regexp.append(board[i][j].getContent());
    				}
    			}
    		}
    	}
    	if(regexp.charAt(regexp.length()-1)=='?')
    			regexp.delete(regexp.length()-2, regexp.length());
    	for(int j=0;j<k;j++)
    		regexp.append(")?");
    	regexp.append("$");
    	System.out.println(regexp.toString());
    	return regexp.toString();
    }
}
