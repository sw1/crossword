package board;

import java.io.Serializable;

import dictionary.CwEntry.Direction;

public class BoardCell implements Serializable {
	private static final long serialVersionUID = 1L;
	Direction d;
	private String content;
	public CellOptions options = new CellOptions();
	public enum State {
		disabled, enabled
	};

	public enum Position {
		Horiz, Vert
	};

	public enum Location {
		Start, End, Inner
	};

	public class CellOptions implements Serializable {
		private static final long serialVersionUID = 1L;
		State state;
		Location location;
		Position position;

		public State getState() {
			return state;
		}

		public void setState(State state) {
			this.state = state;
		}

		public Location getLocation() {
			return location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

		public Position getPosition() {
			return position;
		}

		public void setPosition(Position position) {
			this.position = position;
		}
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public BoardCell copy() {
		try {
			return (BoardCell) clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

}
