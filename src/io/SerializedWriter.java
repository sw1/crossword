package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;

import game.Crossword;

public class SerializedWriter implements Writer {
	String crossDir;
	
	public SerializedWriter(String crossDir)
	{
		this.crossDir=crossDir;
	}

	public void write(Crossword cross) throws FileNotFoundException, IOException {
			long id=getUniqueID();
			ObjectOutputStream obj=new ObjectOutputStream(new FileOutputStream(crossDir+"/"+String.valueOf(id)+".scw"));
			cross.setID(id);
			obj.writeObject(cross);
			obj.close();
	}
	public long getUniqueID() {
		Date date=new Date();
		return date.getTime();
	}

}
