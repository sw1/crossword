package io;

import gui.Observer;

public interface Observable {
    public void notifyObservers(Object obj);
    public void addObserver(Observer o);
    public void removeObserver(Observer o);
}
