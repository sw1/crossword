package io;

import game.Crossword;

import java.io.IOException;
import java.util.LinkedList;

public interface Reader {
	public LinkedList<Crossword> getAllCws() throws IOException, ClassNotFoundException;
}
