package io;

import game.BoardBuilder;
import game.Crossword;
import gui.Observer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import dictionary.InteliCwDB;

public class CwBrowser implements Observable {
	private LinkedList<Crossword> cwList;
	private Reader reader;
	private Writer writer;
    public Reader getReader() {
		return reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

	protected List<Observer> observers = new ArrayList<>();

    @Override
    public void notifyObservers(Object cw) {
        for(Observer o : observers) {
            o.update(cw);
        }
    }

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

	public CwBrowser()
	{
	}
	
	public void save(Crossword crwd) throws FileNotFoundException, IOException
	{
		writer.write(crwd);
	}
	
	public void read() throws IOException, ClassNotFoundException
	{
		cwList=reader.getAllCws();
	}
	
	public Iterator<Crossword> getCwListIterator()
	{
		return cwList.iterator();
	}
	
	
	public Crossword generate(BoardBuilder s, InteliCwDB cwdb) throws game.SimpleBoardBuilder.StrategyException
	{
		Crossword crwd=new Crossword ();
		crwd.setCwDB(cwdb);
		crwd.setBoardBuilder(s);
		crwd.generate();
        notifyObservers(crwd);
		return crwd;
	}

	public Writer getWriter() {
		return writer;
	}

	public void setWriter(Writer writer) {
		this.writer = writer;
	}
}