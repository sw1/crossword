package io;

import java.io.FileNotFoundException;
import java.io.IOException;

import game.Crossword;

public interface Writer {
	public void write(Crossword cross) throws FileNotFoundException, IOException;
	long getUniqueID();
}
