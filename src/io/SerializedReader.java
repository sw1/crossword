package io;

import game.Crossword;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.*;
import java.util.LinkedList;

public class SerializedReader implements Reader {

	String crossDir;
	public SerializedReader(String crosDir)
	{
		this.crossDir=crosDir;
	}
	public LinkedList<Crossword> getAllCws() throws IOException, ClassNotFoundException  
	{
		LinkedList<Crossword> list=new LinkedList<Crossword>();
		
		Path dir = Paths.get(crossDir+"/");
		DirectoryStream<Path> stream =
		     Files.newDirectoryStream(dir, "*.{scw}");
		    for (Path entry: stream) {
		    	
		    	ObjectInputStream obj=new ObjectInputStream(new FileInputStream(entry.toFile()));
		        list.add((Crossword) obj.readObject());
		        obj.close();
		    }
	

		return list;
	}

}
