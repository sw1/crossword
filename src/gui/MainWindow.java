package gui;

import game.Crossword;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import board.Board;
import dictionary.CwEntry;

public class MainWindow implements Printable, Observer{

	private JFrame frame;
	private JTextField cwDir;
	private JButton btnGeneruj;
	private JButton btnDrukuj;
	private JButton btnRozwi;
	private JButton btnWczytaj;
	private JButton btnZapisz;
	private JSpinner spinner;
	private JSpinner spinner_1;
	private JTextPane textPane;
	private CwPanel cwPanel;
	
	public MainWindow() {
		initialize();
	}

	
	private void initialize() {
		frame=new JFrame();
		frame.setBounds(100, 100, 640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Insets margin=new Insets(0,0,0,0);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{218, 188, 0, 0};
		gridBagLayout.rowHeights = new int[]{42, 217, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(0);
		flowLayout.setHgap(0);
		panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Nowa krzy\u017C\u00F3wka", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JLabel lblNewLabel = new JLabel("wys");
		panel.add(lblNewLabel);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(10, 1, 20, 1));
		spinner.setPreferredSize(new Dimension(38, 20));
		spinner.setMinimumSize(new Dimension(38, 20));
		panel.add(spinner);
		
		JLabel lblSzeroko = new JLabel("szer");
		panel.add(lblSzeroko);
		
		spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerNumberModel(10, 1, 20, 1));
		spinner_1.setMinimumSize(new Dimension(38, 20));
		spinner_1.setPreferredSize(new Dimension(38, 20));
		panel.add(spinner_1);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frame.getContentPane().add(panel, gbc_panel);
		
		btnGeneruj = new JButton("Generuj");
		panel.add(btnGeneruj);
		btnGeneruj.setMargin(margin);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "z pliku", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] {114, 34, 74, 0};
		gbl_panel_1.rowHeights = new int[]{21, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		cwDir = new JTextField();
		cwDir.setText( System.getProperty("user.dir"));
		GridBagConstraints gbc_txthomecw = new GridBagConstraints();
		gbc_txthomecw.fill = GridBagConstraints.BOTH;
		gbc_txthomecw.insets = new Insets(0, 0, 0, 5);
		gbc_txthomecw.gridx = 0;
		gbc_txthomecw.gridy = 0;
		panel_1.add(cwDir, gbc_txthomecw);
		cwDir.setColumns(15);
		
		JButton button = new JButton("...");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jFC=new JFileChooser();
				jFC.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int r=jFC.showOpenDialog(frame);
				if(r==JFileChooser.APPROVE_OPTION)
				{
					cwDir.setText(jFC.getSelectedFile().getAbsolutePath());
				}
			}
		});
		button.setMargin(margin);
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.fill = GridBagConstraints.BOTH;
		gbc_button.insets = new Insets(0, 0, 0, 5);
		gbc_button.gridx = 1;
		gbc_button.gridy = 0;
		panel_1.add(button, gbc_button);
		
		btnWczytaj = new JButton("Wczytaj");
		GridBagConstraints gbc_btnWczytaj = new GridBagConstraints();
		gbc_btnWczytaj.fill = GridBagConstraints.BOTH;
		gbc_btnWczytaj.gridx = 2;
		gbc_btnWczytaj.gridy = 0;
		panel_1.add(btnWczytaj, gbc_btnWczytaj);
		btnWczytaj.setMargin(margin);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 0;
		frame.getContentPane().add(panel_1, gbc_panel_1);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_2.getLayout();
		flowLayout_2.setVgap(0);
		flowLayout_2.setHgap(0);
		panel_2.setBorder(new TitledBorder(null, "kontrola", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.anchor = GridBagConstraints.EAST;
		gbc_panel_2.gridx = 2;
		gbc_panel_2.gridy = 0;
		frame.getContentPane().add(panel_2, gbc_panel_2);
		
		btnRozwi = new JButton("Rozwiąż");
		btnRozwi.setMargin(margin);
		panel_2.add(btnRozwi);
		
		btnDrukuj = new JButton("Drukuj");
		btnDrukuj.setMargin(margin);
		panel_2.add(btnDrukuj);
		
		btnZapisz = new JButton("Zapisz");
		btnZapisz.setMargin(margin);
		panel_2.add(btnZapisz);
		
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.insets = new Insets(0, 0, 5, 0);
		gbc_table.gridwidth = 3;
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 1;
		
		cwPanel=new CwPanel();
		
		frame.getContentPane().add(cwPanel, gbc_table);
		
		scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 2;
		frame.getContentPane().add(scrollPane, gbc_scrollPane);
		
		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		textPane.setEditable(false);
	}

    @Override
    public void update(Object arg) {
        showCw((Crossword)arg);
    }

	public void showCw(Crossword cw)
	{
		Board board=cw.getBoard();
		cwPanel.setBoard(board);
		Iterator<CwEntry> iter=cw.getROEntryIter();
		StringBuilder clues=new StringBuilder();
		CwEntry entry;
		if(iter.hasNext())
			iter.next();
		while(iter.hasNext())
		{
			entry=iter.next();
			clues.append(entry.getX()).append(".").append(entry.getY()).append(" ").append(entry.getClue()).append("\n");
		}
		textPane.setText(clues.toString());

	}
	
	public void solve(Crossword cw)
	{
		cwPanel.solveCw();
		
	}

	public JFrame getFrame() {
		return frame;
	}
	public JButton getBtnGeneruj() {
		return btnGeneruj;
	}
	public JButton getBtnDrukuj() {
		return btnDrukuj;
	}
	public JButton getBtnRozwiaz() {
		return btnRozwi;
	}
	public JButton getBtnWczytaj() {
		return btnWczytaj;
	}
	public JButton getBtnZapisz() {
		return btnZapisz;
	}
	public String getDirectory() {
		return cwDir.getText();
	}
	public Integer getCwHeight() {
		return Integer.parseInt(spinner.getValue().toString());
	}
	public Integer getCwWidth() {
		return Integer.parseInt(spinner_1.getValue().toString());
	}
	
	private JScrollPane scrollPane;
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException{
		//TODO
		return Printable.NO_SUCH_PAGE;
	}

}
