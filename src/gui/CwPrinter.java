package gui;

import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.OrientationRequested;

public class CwPrinter {
	public void print(Printable document)
	{ /* Construct the print request specification.
         * The print data is a Printable object.
         * the request additonally specifies a job name, 2 copies, and
         * landscape orientation of the media.
         */
         PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
         aset.add(OrientationRequested.PORTRAIT);
         aset.add(new JobName("My job", null));

         /* Create a print job */
         PrinterJob pj = PrinterJob.getPrinterJob();       
         pj.setPrintable(document);
         /* locate a print service that can handle the request */
         PrintService[] services =
                 PrinterJob.lookupPrintServices();

         if (services.length > 0) {
                 //System.out.println("selected printer " + services[0].getName());
                 try {
                         pj.setPrintService(services[0]);
                        // pj.pageDialog(aset);
                         if(pj.printDialog(aset)) {
                                 pj.print(aset);
                         }
                 } catch (PrinterException pe) { 
                         System.err.println(pe);
                 }
         }
	}
}
