package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

public class CwField extends JComponent implements Cloneable, MouseListener, KeyListener,FocusListener{

	private static final long serialVersionUID = 6798462363179004957L;
	private Character value;
	private Font font;
	private Integer borderSize;
	private Point cwPos;
	public Point getCwPos() {
		return cwPos;
	}
	public void setCwPos(Point cwPos) {
		this.cwPos = cwPos;
	}

	public CwField(Character value, Font font,Integer borderSize){
		this.value=value;
		this.font=font;
		this.borderSize=borderSize;
		setFocusable(true);
		setFocusTraversalKeysEnabled(true);
		addMouseListener(this);
		addKeyListener(this);
		addFocusListener(this);
	}
	public CwField(){
		this(' ',new Font("Serif", Font.PLAIN, 10),1);
	}

	public CwField clone(){
		return new CwField(value,font, borderSize);
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setFont(font);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, font.getSize()+borderSize*2, font.getSize()+borderSize*2);
		if(isFocusOwner()){
			g.setColor(Color.GREEN);
		}else{
			g.setColor(Color.BLACK);
		}
		g.drawRect(0, 0, font.getSize()+borderSize*2, font.getSize()+borderSize*2);
		g.setColor(Color.BLACK);
		g.drawString(value.toString(), borderSize, borderSize+font.getSize());
	}
	
	public void setValue(char value){
		this.value=value;
		repaint();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		this.requestFocusInWindow();
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		System.out.println(arg0.getSource().toString());
		if(Character.isAlphabetic(arg0.getKeyChar())){
			value=Character.toUpperCase(arg0.getKeyChar());
			transferFocus();
			repaint();
		}	
	}
	@Override
	public void keyReleased(KeyEvent arg0) {		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {		
	}
	@Override
	public void focusGained(FocusEvent arg0) {
		repaint();
	}
	@Override
	public void focusLost(FocusEvent arg0) {
		repaint();
		
	}
}
