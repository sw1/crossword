package gui;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import board.Board;

public class CwPanel extends JPanel implements MouseListener{

	private static final long serialVersionUID = 2476874951876127098L;
	Integer fontSize=20;
	Integer borderSize=1;
	Board cwBoard;
	List<CwField> fieldList;
	public CwPanel(){
		setLayout(null);
		addMouseListener(this);
	}
	public void setBoard(Board cwBoard){
		this.removeAll();
		fieldList=new ArrayList<CwField>();
		this.cwBoard=cwBoard;
		CwField fieldPrototype=new CwField(' ',new Font("Serif", Font.PLAIN, fontSize),borderSize);
		int lastX=0, lastY=0;
		for(int i=0; i<cwBoard.getHeight();i++){
			for(int j=0;j<cwBoard.getWidth();j++){
				if(cwBoard.getCell(j, i)!=null){
					CwField newField=fieldPrototype.clone();
					newField.setLocation(new Point(lastX,lastY));
					newField.setSize(fontSize+borderSize*2+1, fontSize+borderSize*2+1);
					fieldList.add(newField);
					lastX+=fontSize+borderSize*2;
					newField.setCwPos(new Point(j,i));
					this.add(newField);
				}
			}
			lastY+=fontSize+borderSize*2;
			lastX=0;
		}
		this.repaint();
	}
	
	public void solveCw(){
		for (CwField field:fieldList){
			field.setValue(Character.toUpperCase(cwBoard.getCell(field.getCwPos().x,field.getCwPos().y).getContent().charAt(0)));
		}
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println(e.getSource().toString());
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
