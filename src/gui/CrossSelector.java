package gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class CrossSelector extends JDialog {

	private static final long serialVersionUID = 181092231717241901L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox<Long> comboBox;
	private JButton okButton;

	public CrossSelector() {
		setResizable(false);
		setBounds(100, 100, 267, 146);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 263, 121);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);

		JLabel lblWybierzKrzywk = new JLabel("Wybierz krzyżówkę");
		lblWybierzKrzywk.setBounds(12, 12, 138, 15);
		contentPanel.add(lblWybierzKrzywk);

		comboBox = new JComboBox<Long>();
		comboBox.setBounds(12, 39, 233, 24);
		contentPanel.add(comboBox);

		JPanel buttonPane = new JPanel();
		buttonPane.setBounds(12, 75, 239, 35);
		contentPanel.add(buttonPane);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		okButton = new JButton("OK");
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);

	}

	public JButton getOkButton() {
		return okButton;
	}

	public JComboBox<Long> getComboBox() {
		return comboBox;
	}
}
