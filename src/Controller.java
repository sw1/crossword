import dictionary.InteliCwDB;
import game.AdvancedBoardBuilder;
import game.Crossword;
import game.SimpleBoardBuilder;
import game.BoardBuilder.StrategyException;
import gui.CrossSelector;
import gui.CwPrinter;
import gui.MainWindow;
import io.CwBrowser;
import io.SerializedReader;
import io.SerializedWriter;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InvalidClassException;
import java.util.Iterator;


public class Controller {
	private MainWindow window;
	private Crossword cw;
	private CwBrowser cwBrowser;
	private CwPrinter printer;

	
	public static void main(String[] args) {
		Controller controller = new Controller();
		controller.start();
	}
	
	
	public void start(){
		try {
			window=new MainWindow();
			printer = new CwPrinter();
			window.getFrame().setVisible(true);
			window.getBtnGeneruj().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					generate();
				}
			});
			window.getBtnWczytaj().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					load();
				}
			});
			window.getBtnZapisz().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					save();
				}
			});
			window.getBtnRozwiaz().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					solve();
				}
			});
			window.getBtnDrukuj().addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					print();
				}
			});
			cwBrowser = new CwBrowser();
            cwBrowser.addObserver(window);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(window.getFrame(),
				    e.getMessage()+"\n"+e.getStackTrace(),
				    "Błąd",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
	
    private void generate()
	{
		InteliCwDB cwDb=new InteliCwDB("cwdb.txt");
		try {

			cw=cwBrowser.generate(new SimpleBoardBuilder(window.getCwWidth(), window.getCwHeight()), cwDb);
		} catch (StrategyException e) {
			JOptionPane.showMessageDialog(window.getFrame(),
				    "Błąd generowania krzyżówki",
				    "Błąd",
				    JOptionPane.WARNING_MESSAGE);

		}
	}
	
	private void load()
	{
		SerializedReader reader=new SerializedReader(window.getDirectory()) ;
		try {
		 	cwBrowser.setReader(reader);
			cwBrowser.read();
		final CrossSelector dial=new CrossSelector();
		final JComboBox<Long> cmbBox=dial.getComboBox();
		Iterator<Crossword> iter= cwBrowser.getCwListIterator() ;
		while(iter.hasNext())
		{
			cmbBox.addItem((Long)iter.next().getID());
		}
		
		dial.getOkButton().addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				Iterator<Crossword> iter= cwBrowser.getCwListIterator() ;
				Crossword cw;
				while(iter.hasNext())
				{
					cw=iter.next();
					if(((Long)cmbBox.getSelectedItem()).longValue()==cw.getID())
					{
						window.showCw(cw);
						dial.dispose();
						return;
					}
				}
				dial.dispose();
		}
		});
		dial.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dial.setVisible(true);
		} catch (ClassNotFoundException|InvalidClassException e1) {
			JOptionPane.showMessageDialog(window.getFrame(), "Błąd wczytywania pliku");
		} catch(IOException e1)
		{
			JOptionPane.showMessageDialog(window.getFrame(), "Błędna ścieżka do pliku");
		}
		// TODO show menu to choose crossword
	}
	private void save()
	{
		if(cw==null)
		{
			JOptionPane.showMessageDialog(window.getFrame(),
				    "Brak wygenerowanej krzyżówki",
				    "Błąd",
				    JOptionPane.WARNING_MESSAGE);
			return;
		}
		SerializedWriter writer=new SerializedWriter(window.getDirectory());
		try {
			cwBrowser.setWriter(writer);
			cwBrowser.save(cw);
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(window.getFrame(),
				    "Błąd ścieżki pliku",
				    "Błąd",
				    JOptionPane.WARNING_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(window.getFrame(),
				    "Błąd zapisu krzyżówki",
				    "Błąd",
				    JOptionPane.WARNING_MESSAGE);
		}
	}
	
	private void solve() {
		if(cw==null)
		{
			JOptionPane.showMessageDialog(window.getFrame(),
				    "Brak wygenerowanej krzyżówki",
				    "Błąd",
				    JOptionPane.WARNING_MESSAGE);
			return;
		}
		window.solve(cw);
	}
	
	private void print()
	{
		if(cw==null)
		{
			JOptionPane.showMessageDialog(window.getFrame(),
				    "Brak wygenerowanej krzyżówki",
				    "Błąd",
				    JOptionPane.WARNING_MESSAGE);
			return;
		}
		printer.print(window);
	}


	
}
