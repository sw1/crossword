package dictionary;

import java.util.LinkedList;
import java.util.Random;


public class InteliCwDB extends CwDB implements java.io.Serializable {
	private static final long serialVersionUID = 1407339118608898119L;
	public InteliCwDB(String filename)
	{
		super(filename);
	}
	public LinkedList<Entry> findAll(String pattern)
	{
		LinkedList<Entry> result=new LinkedList<>();
		for(Entry e: dict)
		{
			if(e.getWord().matches(pattern))
				result.add(e);
		}
		return result;
	}
	public LinkedList<Entry> findAllLength(int length)
	{
		LinkedList<Entry> result=new LinkedList<>();
		for(Entry e: dict)
		{
			if(e.getWord().length()==length)
				result.add(e);
		}
		return result;
	}
	public Entry getRandom()
	{
		Random rand=new Random();
		return dict.get(rand.nextInt(dict.size()-1));
	}
	public Entry getRandom(int length)
	{
		LinkedList<Entry> dict2=findAllLength(length);
		Random rand=new Random();
		if(dict2.size()-1<=0)
			return null;
		return dict2.get(rand.nextInt(dict2.size()-1));
	}
	public Entry getRandom(String pattern)
	{
		LinkedList<Entry> dict2=findAll(pattern);
		Random rand=new Random();
		if(dict2.size()-1<=0)
			return null;
		return dict2.get(rand.nextInt(dict2.size()-1));
	}
	
}
