package dictionary;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.LinkedList;

public class CwDB implements java.io.Serializable{
	private static final long serialVersionUID = -1855598157340500387L;
	protected LinkedList<Entry> dict;
	public CwDB(String filename)
	{
		dict=new LinkedList<Entry>();
		createDB(filename);
	}
	public void add(String word, String clue)
	{
		Entry e=new Entry(word,clue);
		dict.add(e);
	}
	public Entry get(String word)
	{
		for (Entry d :dict)
		{
			if(d.getWord().equals(word))
				return d;
		}
		return null;
	}
	public void remove(String word)
	{
		for (Entry d :dict)
		{
			if(d.getWord().equals(word))
				dict.remove(d);
		}
	}
	public void saveDB(String filename)
	{
		PrintWriter outputStream = null;
		try {
				outputStream = new PrintWriter(new FileWriter(filename));
			for (Entry d :dict)
			{
				printEntry(outputStream, d);
			}
			if (outputStream != null)
                outputStream.close();
		}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		
	}
	private void printEntry(PrintWriter outputStream, Entry d) {
		outputStream.write(d.getWord());
		outputStream.write(d.getClue());
	}
	
	public int getSize()
	{
		return dict.size();
	}
	
	protected void createDB(String filename)
	{
		BufferedReader inputStream = null;
		String l1,l2;
        try {
            inputStream = new BufferedReader(new FileReader(filename));
            while ((l1 = inputStream.readLine()) != null && (l2 = inputStream.readLine()) != null) {
                add(l1,l2);
            }

            if (inputStream != null) {
                inputStream.close();
            }
        }
        catch(IOException e)
        {
        	e.printStackTrace();
        }

	}
}
