package dictionary;

public class Entry implements java.io.Serializable{
	private static final long serialVersionUID = 7099379369719944830L;
	private String word, clue;
	public Entry(String word, String clue)
	{
		this.word=word;
		this.clue=clue;
	}
	public String getWord()
	{
		return word;
	}
	public String getClue()
	{
		return clue;
	}
}
