package dictionary;

public class CwEntry extends Entry implements java.io.Serializable{
	private static final long serialVersionUID = 2281894895008561344L;
	public enum Direction {HORIZ,VERT};
    public CwEntry(String word, String clue, int x, int y, Direction d) {
		super(word, clue);
		this.x=x;                                 
		this.y=y;
		this.d=d;
	}
	private int x;// - określa poziomą współrzędna hasła w krzyżówce
    private int y;// - określa pionową współrzędna hasła w krzyżówce
    private Direction d;// - gdzie Direction jest typem enumerte o wartościach HORIZ i VERT, określającym, czy dane hasło jest umieszczone poziomo, czy pionowo w krzyżówce
    public int getX()
    {
    	return x;
    }
    public int getY()
    {
    	return y;
    }
    public Direction getDir()
    {
    	return d;
    }

}
